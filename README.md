# Sugar Chat

Sugar Chat is a demo chat room script aimed at web developers to provide a good starting point of how to make a chat room using javascript, jquery and php.

* [View the changelog.](https://gitlab.com/desbest/sugar-chat/-/wikis/Changelog)
* [Project or script web page](http://desbest.com/projects/sugar-chat)

## Technologies used
* Long Polling
* GUN Database (coming soon)
* Websockets using wretch library (coming soon)

## Credits

* [Jaan](http://forum.codecall.net/topic/49763-simple-chatroom-with-php-jquery/) from Codecall Forum
* [desbest](http://desbest.com)
* [Lermex](https://github.com/Lermex)

MIT License