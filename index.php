<?php

// Let's start session and let's include our files
include("includes/config.php");    
include("includes/actions.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Sugar Chat</title>
<script src="includes/jquery.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>
<script src="includes/process.js" type="text/javascript"></script>
<link href="includes/style.css" rel="stylesheet" type="text/css" />
</head>

<body>

<?php

// Now if our username is "Guest"
// Let's display the login form
if(get_username() == "Guest"){

echo "<td>"
.login_form()
."</td>";

// If it isn't let's display the chatroom    
}else{

?>

<table width="900" border="0" align="center" id="maintable">
<tr>
<td class="tablebottom" colspan="2" id="userbox">
<?php 

// Let's say greetings to the user and display the "Log out link"
echo "Welcome: ".get_username()."! (".get_link().")"; 

?>
</td>
</tr>
<tr>
<td align="center" class="tablebottom">
<div id="nointernet" class="notice redtextbox erroriconforinput hideme">Cannot fetch new messages. Is your internet working?</div>
<div id="chatscreen">



<?php

// Now here is the same thing that is in actions file
// Let's select all info from "chat" table
$sql = "SELECT * FROM chat LIMIT 1000";
$query = mysql_query($sql);

// Little error checking again
if(!$query){

echo "Can not get messages from database.";

}else{

// If everything is good let's display
// User's messages
while($row = mysql_fetch_array($query)){

echo "<div align='left'>(".$row['time'].") "."<b>".$row['user'].":</b> ".$row['text']."</div>";

}

}    

?>

</div>
</td>
<td class="middlebox" id="usersbox" rowspan="2">
<div id="onlineusers">

<?php

// This is also like in actions file
// Get all info from "auth" table
$sql = "SELECT * FROM auth";
$query = mysql_query($sql);

// Check for errors
if(!$query){

echo "Can not get users from database. ".mysql_error();

}else{

// If everything is fine
// Let's display active users
while($row = mysql_fetch_array($query)){

echo "<div class='user'>".$row['user']."</div>";

}

}

?>

</div>
</td>
</tr>

<tr>
<td colspan="2" id="messagebox">
<form action="">
<input type="text" id="text" name="chattxt" autocomplete="off" /> <input type="submit" id="submitbutton" value="Send" /> 
</form>
</td>
</tr>
</table>

<?php

}

?>

</body>
</html>