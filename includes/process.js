// If page is loaded let's load our functions
$(function(){

var isScrolledToBottom = false; 
// https://jsfiddle.net/desbest/fg0a7hLy/
// https://stackoverflow.com/a/65229395/337306
// Lermex's solution
// Code written by Jann, Lermex and


// Now let's load our messages
function load_messages(){
	// Let's use AJAX to get them from our actions file
	$.ajax({
		url: "includes/actions.php?act=getmessages",
		cache: false,
		success: function(html){
			// Let's get old posts from there
			// Where user's posts are shown
			$("#chatscreen").html(html); 

			$("#nointernet").addClass("hideme");
			updateScr(); //scroll to bottom of page
		}, error: function (xhr, ajaxOptions, thrownError) {
      $("#nointernet").removeClass("hideme");

      for (var i = 0; i < 3; i++ ) {
        $("#nointernet")
            .animate( { borderTopColor: "#BAFDA0" }, 1200 )
            .animate( { borderTopColor: "#B9A0FD" }, 1200 )
            .animate( { borderTopColor: "#FDA0EE" }, 1200 )
        ;
      }
    }
    	//pulsate animation

	});
		//and maybe test out dummy content
}


var out = document.getElementById("chatscreen");
var c = 0;

$("#chatscreen").on('scroll', function(){
		// console.log(out.scrollHeight);
    isScrolledToBottom = out.scrollHeight - out.clientHeight <= out.scrollTop + 32;
});

function updateScr() {
		// allow 1px inaccuracy by adding 1
    //console.log(out.scrollHeight - out.clientHeight,  out.scrollTop + 1);
    	/*
    	//spit out dummy content
    	var newElement = document.createElement("div");
    	newElement.innerHTML = c++;
    	out.appendChild(newElement);
    	*/
    
    // console.log(isScrolledToBottom);
    // scroll to bottom if isScrolledToBotto
    if(isScrolledToBottom) {out.scrollTop = out.scrollHeight - out.clientHeight; }
}


// If submit button has been clicked
$("#submitbutton").click(function(){
	// Let's get what user has entered to the text field
	var message = $("#text").val();

	// Now let's post this text
	jQuery.ajax({
      type: "POST",
      url: "includes/actions.php?act=post&text="+message+"",
      success: function (response) {
        // When user's text is posted
		// Remove it from the text field    
		$("#text").val("");
		$("#text").removeClass("redtextbox");
		$("#text").removeClass("erroriconforinput");
		// alert("123");

		// $("#chatscreen").html(html); 
		$("#chatscreen").animate({ scrollTop: $('#chatscreen').prop("scrollHeight")}, 1000);
      },
      error: function (xhr, ajaxOptions, thrownError) {
        $("#text").addClass("redtextbox");
        $("#text").addClass("erroriconforinput");
        // alert("error");
      }
    });

	return false;
});



// Now let's load chatroom's active users    
function load_users(){
	// Let's use AJAX also to get chatroom's users
	$.ajax({
	url: "includes/actions.php?act=getusers",
	cache: false    
	});
}

// Now let's set the time when we will
// Check for new messages and new users
// First, we set the time of our posts
var add = setInterval(updateScr, 1000);
setInterval(load_messages, 1000); // 3000-1500-1000-500-300
setInterval(load_users, 5000);    

});
