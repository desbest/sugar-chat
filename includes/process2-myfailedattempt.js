// http://forum.codecall.net/topic/49763-simple-chatroom-with-php-jquery/
// but with bug fixes by desbest

// If page is loaded let's load our functions

$(function(){


// If submit button has been clicked
$("#submitbutton").click(function(){

// Let's get what user has entered to the text field
var message = $("#text").val();

// Now let's post this text
$.post("actions.php?act=post", {

text: message

});

//scroll down to bottom
var intElemScrollHeight = document.getElementById("chatscreen").scrollHeight;
var newheight =   intElemScrollHeight - 12;
$("#chatscreen").animate({ scrollTop: newheight }, 300); 
//console.log("scrolled down to bottom");


// When user's text is posted
// Remove it from the text field    
$("#text").val("");

return false;

});


//lets save some bandwidth
function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
}


// Now let's load our messages
function load_messages(){

// Let's use AJAX to get them from our actions file
$.ajax({

url: "actions.php?act=getmessages",
cache: false,
success: function(html){

// Let's get old posts from there
// Where user's posts are shown
$("#chatscreen").html(html); 
console.log("update posts view");

// Now let's get this area's height where
// All posts are displayed and then let's animate
// It little bit so the scrollbar is always 
// Scrolled down so you can see the new posts    
var newheight = $("#chatscreen").attr("scrollHeight") - 0;

//$("#chatscreen").animate({ scrollTop: newheight }, 'fast'); 

// keep the scrollbar at the bottom when new content appears except when the user scrolls up
// attepmpt 1
$('#chatscreen').scroll(function() {
    var scrolledto = $("#chatscreen")[0].scrollHeight - $(this).scrollTop();
    var outerheight = $("#chatscreen").outerHeight();
    var hundredabove = outerheight + 100;
    if (scrolledto < hundredabove){
        var intElemScrollHeight = document.getElementById("chatscreen").scrollHeight;
        var newheight =   intElemScrollHeight - 12;
        //$("#chatscreen").animate({ scrollTop: newheight }, 300); 
    }
    console.log("scrolledto is "+scrolledto+" // outerheight is "+outerheight+" // hundredabove is "+hundredabove);
});

// attempt 2
$('#chatscreen').scroll(function() {
if ($(this)[0].scrollHeight - $(this).scrollTop() >= $(this).outerHeight() - 24)  //scrolled to bottom
{
    //dodat();
}
}); 

},

});

}

// Now let's load chatroom's active users    
function load_users(){

// Let's use AJAX also to get chatroom's users
$.ajax({

url: "actions.php?act=getusers",
cache: false    

});

}

// Now let's set the time when we will
// Check for new messages and new users
// First, we set the time of our posts
// setInterval(load_messages, 500) loads our posts every 0.5 seconds
// setInterval(load_users, 5000) loads our posts every 5 seconds

setInterval(load_users, 5000);    
setInterval(load_messages,1000); // change to 300 to show the latest message you sent after pressing enter
                                // leaving it on 1000 shows the second to last message
});
