<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit5fd05567166fbc46aaacd7cf33a54a31
{
    public static $prefixesPsr0 = array (
        'W' => 
        array (
            'Wrench' => 
            array (
                0 => __DIR__ . '/..' . '/wrench/wrench/lib',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixesPsr0 = ComposerStaticInit5fd05567166fbc46aaacd7cf33a54a31::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
